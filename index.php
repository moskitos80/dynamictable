<!DOCTYPE html>
<html>
<head>
    <title>Dynamic table example</title>
</head>
<body>

<h1>Динамическая таблица.</h1>

<p>
    Автор: Комаров Артем
    <br />email: arty-komarov@yandex.ru
    <br />website: <a href="http://php-zametki.ru">http://php-zametki.ru</a>
</p>

<?php /* ПРИМЕР РАЗБОРА ДАННЫХ НА СЕРВЕРЕ */?>
<?php if (!empty($_POST)) : ?>

    <?php $i = 1; ?>

    <hr />
    <h3>Получена структура данных:</h3>
    <pre><?php print_r($_POST) ?></pre>
    <hr />
    <h3>Приняты следующие данные:</h3>
    <dl>
        <?php foreach ($_POST as $key => $value) :
            if (gettype($key) === "integer") : ?>
                <dt>Строка № <?php echo $i++ ?> : </dt>
                    <dd>
                        <strong>Текстовое поле : </strong><?php echo $value['text'] ?><br />
                        <strong>Select:</strong> <?php echo $value['select'] ?><br />
                        <strong>Radio:</strong> <?php echo $value['radio'] ?><br />
                        <?php if (isset($value['checkbox'])) : ?>
                            <strong>Checkbox: </strong><?php echo $value['checkbox'] ?><br />
                        <?php endif ?>
                        <?php if (isset($_POST['glob-radio']) && $_POST['glob-radio'] == $key) : ?>
                            <strong>Global radio: </strong>CHECKED<br />
                        <?php endif ?>
                        <br />
                    </dd>
            <?php endif ?>
        <?php endforeach ?>
    </dl>
<?php endif ?>

<!--  ДЕМО  //-->

<form method="post" action="">
    <table width="800" border="1" cellspacing="0" cellpadding="5">
        <thead>
            <tr>
                <th scope="col">Текстовое поле</th>
                <th scope="col">checkbox</th>
                <th scope="col">Элемент-список</th>
                <th scope="col">Радио-кнопка уровня таблицы</th>
                <th scope="col">Радио-кнопка уровня строки</th>
                <th scope="col">&nbsp;</th>
            </tr>
        </thead>
        <tbody id="dynamic">
            <tr>
                <td>
                    <label>
                        <input type="text" name="text" value="Значение 1">
                    </label>
                </td>
                <td>
                    <label>
                        <input type="checkbox" name="checkbox" value="Значение 1">
                        Some checkbox
                    </label>
                </td>
                <td>
                    <label>
                        <select name="select">
                            <option value="value-1">value-1</option>
                            <option value="value-2">value-2</option>
                            <option value="value-3">value-3</option>
                        </select>
                    </label>
                </td>
                <td>
                    <label>
                        <input type="radio" name="glob-radio" class="glob">
                        THIS ROW
                    </label>
                </td>
                <td>
                    <label>
                        <input type="radio" name="radio" value="no" checked>
                        NO
                    </label>
                    <label>
                        <input type="radio" name="radio" value="yes">
                        YES
                    </label>
                </td>
                <td>
                    <button type="button" class="add">+</button>
                    <button type="button" class="del">-</button>
                </td>
            </tr>
        </tbody>
    </table>
    <input name="sub" type="submit" value="SEND" style="margin: 10px">
</form>

<hr/>

<h3>Подключение:</h3>

<p>
    Скрипту достаточно указать в разметке первую строку содержащую нужные элементы
    скрипт будет добавлять строки клонируя её. И не даст удалить последнюю строку.
</p>

<pre>
&lt;form method=&quot;post&quot; action=&quot;&quot;&gt;
    &lt;table&gt;
        &lt;thead&gt;
            &lt;tr&gt;
                &lt;th scope=&quot;col&quot;&gt;Текстовое поле&lt;/th&gt;
                &lt;th scope=&quot;col&quot;&gt;checkbox&lt;/th&gt;
                &lt;th scope=&quot;col&quot;&gt;Элемент-список&lt;/th&gt;
                &lt;th scope=&quot;col&quot;&gt;Радио-кнопка уровня таблицы&lt;/th&gt;
                &lt;th scope=&quot;col&quot;&gt;Радио-кнопка уровня строки&lt;/th&gt;
                &lt;th scope=&quot;col&quot;&gt;&amp;nbsp;&lt;/th&gt;
            &lt;/tr&gt;
        &lt;/thead&gt;
        &lt;tbody <strong style="color:green">id=&quot;dynamic&quot;</strong>&gt;
           <strong style="color:darkmagenta">&lt;!-- ЭТА СТРОКА БУДЕТ ШАБЛОНОМ ДЛЯ ВНОВЬ СОЗДАВАЕМЫХ //--&gt;
            &lt;tr&gt;</strong>
                &lt;td&gt;
                    &lt;label&gt;
                        &lt;input type=&quot;text&quot; name=&quot;text&quot; value=&quot;Значение 1&quot;&gt;
                    &lt;/label&gt;
                &lt;/td&gt;
                &lt;td&gt;
                    &lt;label&gt;
                        &lt;input type=&quot;checkbox&quot; name=&quot;checkbox&quot; value=&quot;Значение 1&quot;&gt;
                        Some checkbox
                    &lt;/label&gt;
                &lt;/td&gt;
                &lt;td&gt;
                    &lt;label&gt;
                        &lt;select name=&quot;select&quot;&gt;
                            &lt;option value=&quot;value-1&quot;&gt;value-1&lt;/option&gt;
                            &lt;option value=&quot;value-2&quot;&gt;value-2&lt;/option&gt;
                            &lt;option value=&quot;value-3&quot;&gt;value-3&lt;/option&gt;
                        &lt;/select&gt;
                    &lt;/label&gt;
                &lt;/td&gt;
                &lt;td&gt;
                    &lt;label&gt;
                        &lt;input type=&quot;radio&quot; name=&quot;glob-radio&quot; <strong style="color:blue">class=&quot;glob&quot;</strong>&gt;
                        THIS ROW
                    &lt;/label&gt;
                &lt;/td&gt;
                &lt;td&gt;
                    &lt;label&gt;
                        &lt;input type=&quot;radio&quot; name=&quot;radio&quot; value=&quot;no&quot; checked&gt;
                        NO
                    &lt;/label&gt;
                    &lt;label&gt;
                        &lt;input type=&quot;radio&quot; name=&quot;radio&quot; value=&quot;yes&quot;&gt;
                        YES
                    &lt;/label&gt;
                &lt;/td&gt;
                &lt;td&gt;
                    &lt;button type=&quot;button&quot; <strong style="color:red">class=&quot;add&quot;</strong>&gt;+&lt;/button&gt;
                    &lt;button type=&quot;button&quot; <strong style="color:red">class=&quot;del&quot;</strong>&gt;-&lt;/button&gt;
                &lt;/td&gt;
            <strong style="color:darkmagenta">&lt;/tr&gt;</strong>
        &lt;/tbody&gt;
    &lt;/table&gt;
    &lt;input name=&quot;sub&quot; type=&quot;submit&quot; value=&quot;SEND&quot; style=&quot;margin: 10px&quot;&gt;
&lt;/form&gt;

&lt;script src=&quot;dynamicTable.js&quot;&gt;&lt;/script&gt;
&lt;script&gt;
    new DynamicTable( document.getElementById(&quot;dynamic&quot;) );
&lt;/script&gt;
</pre>

<h3>Условия:</h3>

<p>
    Кнопки, удаления/добавления строк в разметке строки должны иметь строго
    определенные классы: <br/>
    <strong style="color:red">add</strong> - кнопка добавляющая строку<br/>
    <strong style="color:red">del</strong> - кнопка удаляющая строку<br/>
    Кнопки могут располагаться в любой ячейке строки.
</p>

<p>
    Радио-кнопки которые группируются в пределах одной ячейки определяются
    как обычно. Радио-кнопки которые группируются в пределах всей таблицы должны
    обязательно иметь класс <strong style="color:blue">glob</strong>
</p>

<hr/>

<h3>Пример структуры данных, отправляемой на сервер:</h3>

<pre>
Array
(
    [0] => Array
        (
            [text] => Значение 1
            [checkbox] => Значение 1
            [select] => value-1
            [radio] => yes
        )

    ... ,

    [glob-radio] => 2   // Ключ элемента, представл. строку, где была выбрана глобальная радио-кнопка

    [3] => Array
        (
            [text] => Значение 3
            [checkbox] => Значение 1
            [select] => value-3
            [radio] => no
        )

    [sub] => SEND
)
</pre>

<p>
    Внимание! Индексы массива ни в коей мере не отражают порядка строк,
    они всего лишь служат для уникальной идентификации строк в
    существующем наборе данных. Порядковые номера строк таблицы на сервере определить
    не возможно! Массив может содержать как числовые индексы, так и строковые.
    Причем числовые индексы это элементы (подмассивы), представляющие строки таблицы,
    с соответствующими данными, а не числовые индексы это другие элементы уровня таблицы.
    Радио-кнопки уровня таблицы (те что имеют класс <strong style="color:blue">glob</strong>)
    будут представлены как корневые элементы массива, а радио-кнопки уровня
    ячеек будут представлены как элементы подмассивов наряду с остальными элементами.
</p>

<h3>Пример разбора структуры данных на сервере:</h3>

<pre>
&lt;?php if (!empty($_POST)) : ?&gt;

    &lt;?php $i = 1; ?&gt;

    &lt;dl&gt;
        &lt;?php foreach ($_POST as $key =&gt; $value) :
            if (gettype($key) === &quot;integer&quot;) : ?&gt;
                &lt;dt&gt;Строка № &lt;?php echo $i++ ?&gt; : &lt;/dt&gt;
                    &lt;dd&gt;
                        &lt;strong&gt;Текстовое поле : &lt;/strong&gt;&lt;?php echo $value['text'] ?&gt;&lt;br /&gt;
                        &lt;strong&gt;Select:&lt;/strong&gt; &lt;?php echo $value['select'] ?&gt;&lt;br /&gt;
                        &lt;strong&gt;Radio:&lt;/strong&gt; &lt;?php echo $value['radio'] ?&gt;&lt;br /&gt;
                        &lt;?php if (isset($value['checkbox'])) : ?&gt;
                            &lt;strong&gt;Checkbox: &lt;/strong&gt;&lt;?php echo $value['checkbox'] ?&gt;&lt;br /&gt;
                        &lt;?php endif ?&gt;
                        &lt;?php if ($_POST['glob-radio'] == $key) : ?&gt;
                            &lt;strong&gt;Global radio: &lt;/strong&gt;CHECKED&lt;br /&gt;
                        &lt;?php endif ?&gt;
                        &lt;br /&gt;
                    &lt;/dd&gt;
            &lt;?php endif ?&gt;
        &lt;?php endforeach ?&gt;
    &lt;/dl&gt;
&lt;?php endif ?&gt;
</pre>

<script src="dynamicTable.js"></script>
<script>
    new DynamicTable( document.getElementById("dynamic") );
</script>
</body>
</html>